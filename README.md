# Ali Twitter

Twitter-like web app used Rails-Framemwork which can do:

1. Create a tweet
2. Delete a tweet
3. List tweets


## Ruby version

The required Ruby version is **2.6.5**


## Database migration

```bash
rails db:migrate
```


## How to run the test suite

```bash
rails test
```

## How to run

```bash
rails server
```

Then you can access from browser with `http://localhost:3000/tweets`


## How to build Docker Image Artefact
You should have installed [Docker](https://docs.docker.com/install/) in your machine

To build Docker Image with:
```bash
docker build -t <IMAGE_NAME> .
```

To run Docker Image in container with:
```bash
docker container run -p 3000:3000 <IMAGE_NAME>
```

Then you can access from browser with `http://localhost:3000/tweets`


## How to run Kubernetes
You should have installed [Kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/) and [Minikube](https://kubernetes.io/docs/setup/learning-environment/minikube/#interacting-with-your-cluster) in your machine

Run all kubernetes yaml with:
```bash
kubectl apply -f kube/<file_name>.yml
```

To show url with:
```bash
minikube service alitwitter --url
```

And then you can copy the url to your browser
